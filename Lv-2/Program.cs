﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lv_2
{
    class Program
    {
        static void Main(string[] args)
        {
            //1.ZADATAK
            List<Die> die = new List<Die>();
            DiceRoller diceRoller = new DiceRoller();
            for (int i = 0; i < 20; i++)
            {
                die.Add(new Die(6));
            }

            foreach (Die dice in die)
            {
                diceRoller.InsertDie(dice);
            }
            diceRoller.RollAllDice();

            IList<int> list = diceRoller.GetRollingResults();
            foreach (int a in list)
            {
                Console.WriteLine(a.ToString());
            }
            //2.ZADATAK
            List<Die> die1 = new List<Die>();
            DiceRoller diceRoller1 = new DiceRoller();
            Random randomGenerator = new Random();
            for (int i = 0; i < 20; i++)
            {
                die.Add(new Die(6, randomGenerator));
            }

            foreach (Die dice in die)
            {
                diceRoller.InsertDie(dice);
            }
            diceRoller.RollAllDice();

            IList<int> list1 = diceRoller.GetRollingResults();
            foreach (int a in list)
            {
                Console.WriteLine(a.ToString());
            }


        }
    }
}
